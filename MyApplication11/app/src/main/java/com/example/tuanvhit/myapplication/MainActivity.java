package com.example.tuanvhit.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText so1, so2;
    TextView ketqua;
    Button btnsum;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        so1 = (EditText) findViewById(R.id.so1);
        so2 = (EditText) findViewById(R.id.so2);
        ketqua = (TextView) findViewById(R.id.textketqua);
        btnsum = (Button) findViewById(R.id.btntong);
        btnsum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String chuoi1 = so1.getText().toString();
                int so1 = Integer.parseInt(chuoi1);
                String chuoi2 = so2.getText().toString();
                int so2 = Integer.parseInt(chuoi2);
                int tong = so1 + so2;
                ketqua.setText(String.valueOf(tong));
            }
        });
    }
}
