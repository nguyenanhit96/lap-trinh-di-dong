package com.example.tuanvhit.intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class GoogleWebview extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_webview);

        WebView browser = (WebView) findViewById(R.id.webview);
        browser.loadUrl("http://www.google.com.vn");

    }
}
