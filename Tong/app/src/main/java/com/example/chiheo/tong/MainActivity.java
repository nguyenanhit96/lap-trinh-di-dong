package com.example.chiheo.tong;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText edtSo1, edtso2;
    Button btnTong;
    TextView txtvKetQua;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtSo1= (EditText)findViewById(R.id.editTextSo1);
        edtso2= (EditText)findViewById(R.id.editTextSo2);
        txtvKetQua= (TextView)findViewById(R.id.textViewKetQua);
        btnTong= (Button)findViewById(R.id.buttonTong);
        btnTong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String chuoi1= edtSo1.getText().toString();
                int so1 = Integer.parseInt(chuoi1);

                String chuoi2 = edtso2.getText().toString();
                int so2 = Integer.parseInt(chuoi2);

                int tong = so1 + so2;
                txtvKetQua.setText(String.valueOf(tong));

            }
        });

    }
}
