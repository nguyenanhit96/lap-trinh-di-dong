package com.example.thanhhien.myproject1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private EditText so1, so2, kq;
    private Button click;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        so1= (EditText) findViewById(R.id.txtso1);
        so2= (EditText) findViewById(R.id.txtso2);
        click= (Button) findViewById(R.id.btnclick);
        kq= (EditText) findViewById(R.id.txtkq);
        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a, b , tong;
                a= Integer.parseInt(so1.getText().toString());
                b= Integer.parseInt(so2.getText().toString());
                tong= a+b;
                kq.setText(String.valueOf(tong));
            }
        });
    }
}
